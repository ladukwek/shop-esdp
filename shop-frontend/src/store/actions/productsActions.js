import {push} from "connected-react-router";
import axios from "../../axiosApi";
import {CREATE_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actionTypes";

export const fetchProductsSuccess = products => {
  return {type: FETCH_PRODUCTS_SUCCESS, products};
};

export const fetchProducts = () => {
  return dispatch => {
    return axios.get("/products").then(response => {
      dispatch(fetchProductsSuccess(response.data));
    });
  };
};

const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = productData => {
  return async dispatch => {
    await axios.post("/products", productData);
    dispatch(createProductSuccess());
    dispatch(push("/"));
  };
};