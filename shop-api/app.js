const express = require("express");
const cors = require("cors");

const app = express();
require("express-ws")(app);

const products = require("./app/products");
const categories = require("./app/categories");
const users = require("./app/users");
const chat = require("./app/chat");
const port = process.env.NODE_ENV === "test" ? 8010 : 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));


app.use("/products", products);
app.use("/categories", categories);
app.use("/users", users);
app.use("/chat", chat);


module.exports = {app, port};
